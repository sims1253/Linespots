from __future__ import division
import subprocess
import math
import re
from collections import defaultdict
import os
from operator import itemgetter


def get_output(*args, **kwargs):
    """
    Retrieves the output from subprocess.check_output readily converted to a
    string. All arguments will be passed through.
    """
    return subprocess.check_output(*args, **kwargs).decode(errors='ignore')


def git_files_at_depth(depth):
    return set(
        get_output(["git", "ls-tree", "HEAD~%d" % depth, "--name-only",
                    "-r"]).strip().splitlines())


def git_checkout_branch(branch):
    process = subprocess.Popen(["git", "checkout", branch],
                               stdout=subprocess.PIPE)
    process.communicate()


def get_git_files():
    return set(
        get_output(["git", "ls-files"]).strip().splitlines())


def count_lines(filename):
    try:
        with open(filename, 'r', errors='ignore') as file:
            return len(file.readlines())
    except (IsADirectoryError, FileNotFoundError):
        return 0


def get_full_commits(depth):
    """

    :param depth: the number of commits to show before the first
    :return: git show sha-depth separated into single commits, separated into
             lines
    """
    raw_commits = get_output(
        ["git", "show", "-{}".format(depth),
         "--format={}".format('\'commit %H%n%ct%n%B\'')]).strip(
        ' \'').splitlines()

    nice_commits = list(get_chunks_by_line_start(raw_commits, "'commit"))
    return nice_commits


def normalize_timestamp(timestamp, repo_start, repo_age):
    """

    :param timestamp:
    :param repo_start:
    :param repo_age:
    :return: The age of the commit related to the oldest commit between 0 and 1
    """
    if repo_age:
        return (timestamp - repo_start) / repo_age
    else:
        return 1


def get_chunks_by_line_start(text, line_start):
    """
    >>> list(get_chunks_by_line_start([
    ... "Commit message - ignore",
    ... "diff --git diff starts here",
    ... "diff content",
    ... "diff --git second diff starts here",
    ... "second diff content"
    ... ], "diff --git")) # doctest: +NORMALIZE_WHITESPACE
    [['diff --git diff starts here', 'diff content'],
    ['diff --git second diff starts here', 'second diff content']]

    >>> list(get_chunks_by_line_start([
    ... "Commit message - ignore",
    ... "@@ -256,7 +256,7 @@ and even more text",
    ... "hunk content",
    ... "@@ -25,7 +25,7 @@",
    ... "second hunk content",
    ... "still second hunk content"
    ... ], "@@")) # doctest: +NORMALIZE_WHITESPACE
    [['@@ -256,7 +256,7 @@ and even more text', 'hunk content'],
    ['@@ -25,7 +25,7 @@', 'second hunk content', 'still second hunk content']]

    :param text:
    :param line_start:
    :return:
    """
    current_list = None
    for line in text:
        if line.startswith(line_start):
            if current_list:
                yield current_list

            current_list = []
        if current_list is not None:
            current_list.append(line)

    if current_list:
        yield current_list


def separate_body_diff(text):
    if text is None:
        return[[], []]
    i = 0
    for line in text:
        if line.startswith("diff --git"):
            body = text[:i]
            diff = text[i:]
            return[body, diff]
        i += 1
    return [[], []]


def score_diff_hunk(hunk_text, old_hunk_scores, old_hunk_average, reldate, fix):
    """

    :param hunk_text:
    :param old_hunk_scores:
    :param old_hunk_average:
    :param reldate:
    :param fix:
    :return:
    """
    new_line_scores = []
    hunk_text.pop(0)
    line_nr = 0
    for line in hunk_text:
        if line.startswith("-"):
            line_nr += 1
        elif line.startswith("+"):
            if fix:
                new_line_scores.append(
                    old_hunk_average +
                    (1 / (1 + math.exp((-12 * reldate) + 12))))
            else:
                new_line_scores.append(0)

        else:
            if line == "\ No newline at end of file":
                pass
            else:
                try:
                    new_line_scores.append(old_hunk_scores[line_nr])
                except:
                    new_line_scores.append(old_hunk_average)
            line_nr += 1
    return new_line_scores


def get_hunk_average_score(filescore, location, length):
    """
    >>> get_hunk_average_score(
    ... [1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 0],
    ... 3,
    ... 3)
    4.0

    :param filescore:
    :param location:
    :param length:
    :return:
    """
    hunk_score = 0
    if length == 0:
        return hunk_score
    return sum(filescore[(location - 1): (length + location - 1)]) / length


def get_hunk_meta(hunk):
    """
    >>> get_hunk_meta(
    ... "@@ -256,7 +256,7 @@ and even more text")
    [256, 7, 256, 7]

    >>> get_hunk_meta(
    ... "@@ -0 +1,7 @@ and even more text")
    [0, 0, 1, 7]

    >>> get_hunk_meta(
    ... "@@ -0 +1 @@ and even more text")
    [0, 0, 1, 1]

    :param hunk:
    :return: [sub_location, sub_length, add_location, add_length]
    """
    raw_values = hunk[hunk.find("-"):]
    values = raw_values[:raw_values.find("@@")].strip()
    subtracted, added = tuple(values.split(" "))

    if len(subtracted) == 2:
        sub_location = int(subtracted.strip("-").split(",")[0])
        sub_length = sub_location
    else:
        sub_location = int(subtracted.strip("-").split(",")[0])
        sub_length = int(subtracted.strip("-").split(",")[1])

    if len(added) == 2:
        add_location = int(added.strip("+").split(",")[0])
        add_length = add_location
    else:
        add_location = int(added.strip("+").split(",")[0])
        add_length = int(added.strip("+").split(",")[1])

    return [sub_location, sub_length, add_location, add_length]


def score_file_diff(file_diff, reldate, line_scores, fix):
    """

    :param file_diff: Git diff of one file
    :param reldate: Normalized age of the commit
    :param line_scores: The line_scores list for the file
    :param fix: true if the current commit is a bugfix
    :return: updated line_scores
    """
    if line_scores == []:
        new_line_scores = []
        current_line = 0
        for hunk in get_chunks_by_line_start(file_diff, "@@"):
            sub_location, sub_length, add_location, add_length = \
                get_hunk_meta(hunk[0])

            if sub_location - 1 > current_line:
                new_line_scores.extend([0] * (sub_location-1-current_line))
                current_line += (sub_location-1-current_line)

            new_line_scores.extend(
                score_diff_hunk(
                    hunk,
                    ([0] * sub_length),
                    0, reldate, fix))
            current_line += sub_length
        return new_line_scores

    else:
        new_line_scores = []
        current_line = 0
        for hunk in get_chunks_by_line_start(file_diff, "@@"):
            sub_location, sub_length, add_location, add_length = \
                get_hunk_meta(hunk[0])

            if sub_location - 1 > current_line:
                new_line_scores.extend(
                    line_scores[current_line: sub_location - 1])
                current_line += ((sub_location - 1) - current_line)

            hunk_average = get_hunk_average_score(
                line_scores, sub_location, sub_length)

            new_line_scores.extend(
                score_diff_hunk(
                    hunk,
                    line_scores[sub_location-1:sub_location+sub_length-1],
                    hunk_average, reldate, fix))
            current_line += sub_length

        if current_line < len(line_scores):
            new_line_scores.extend(line_scores[current_line: len(line_scores)])

        return new_line_scores


def is_fix_commit(body, grep):
    """
    >>> is_fix_commit(
    ... ["docs/index:Index.rst Change Coala to coala",
    ... "Closes #2414"], ["Fixes", "Closes"])
    True

    >>> is_fix_commit(
    ... ["docs/index:Index.rst Change Coala to coala",
    ... "Fixes #2414"], ["Fixes", "Closes"])
    True

    >>> is_fix_commit(
    ... ["docs/index:Index.rst Change Coala to coala",
    ... "This is no fix"], ["Fixes", "Closes"])
    False

    :param body: The commit body to search
    :param grep: The strings to search for
    :return: True if an element of grep is in an element of body
    """
    for line in body:
        if grep.search(line):
            return True
    return False


def get_file_bugspots(depth, grep):
    """

    :param depth: number of commits to look at
    :param grep: search term to find fix commits
    :return: [(dict: filename, line_score),(dict: filename, fix_count)]
    """
    all_files = get_git_files()
    file_dict = defaultdict(float)
    all_commits = get_full_commits(depth)

    oldest = int(all_commits[-1][1].strip('\''))
    repo_age = int(all_commits[0][1].strip('\'')) - oldest

    for commit in reversed(all_commits):
        commit_date = int(commit[1])
        commit_body, commit_diff = separate_body_diff(commit[2:])
        fix = is_fix_commit(commit_body, grep)
        relative_date = normalize_timestamp(commit_date, oldest, repo_age)

        for full_diff in get_chunks_by_line_start(
                commit_diff, "diff --git"):
            filename = full_diff[0].split()[3][2:]
            # Start ignoring coala
            file_dict[filename]
            # Stop ignoring
            if fix:
                if filename in all_files:
                    file_dict[filename] += \
                        (1 / (1 + math.exp((-12 * relative_date) + 12)))

    return file_dict


def get_line_bugspots(depth, grep):
    """

    :param depth: number of commits to look at
    :param grep: search term to find fix commits
    :return: [(dict: filename, line_score),(dict: filename, fix_count)]
    """
    # TODO The absolute fix count does not track file renames
    file_dict = defaultdict(list)
    absolute_fix_count = defaultdict(int)
    weighted_fix_count = defaultdict(float)
    all_commits = get_full_commits(depth)
    oldest = int(all_commits[-1][1].strip('\''))
    repo_age = int(all_commits[0][1].strip('\'')) - oldest

    for commit in reversed(all_commits):
        commit_date = int(commit[1])
        commit_body, commit_diff = separate_body_diff(commit[2:])
        fix = is_fix_commit(commit_body, grep)
        relative_date = normalize_timestamp(commit_date, oldest, repo_age)

        for full_diff in get_chunks_by_line_start(
                commit_diff, "diff --git"):
            filename = full_diff[0].split()[3][2:]
            # Start ignoring coala
            absolute_fix_count[filename]
            weighted_fix_count[filename]
            # Stop ignoring

            if fix:
                absolute_fix_count[filename] += 1
                weighted_fix_count[
                    filename] += (1 / (1 + math.exp((-12 * relative_date) + 12)))

            if full_diff[1].startswith("new file mode"):
                diff = full_diff[5:]
                file_score = []
                file_dict[filename] = score_file_diff(diff, relative_date,
                                                      file_score, fix)
            elif full_diff[1].startswith("deleted file mode"):
                try:
                    del file_dict[filename]
                    del weighted_fix_count[filename]
                except KeyError:
                    pass

            elif full_diff[2].startswith("rename from"):
                diff = full_diff[4:]
                oldfile = full_diff[2].split()[2]
                try:
                    file_score = file_dict.pop(oldfile)
                    weighted_score = weighted_fix_count.pop(oldfile)
                except KeyError:
                    file_score = []
                    weighted_score = 0.0
                file_dict[filename] = score_file_diff(diff, relative_date,
                                                      file_score, fix)
                weighted_fix_count[filename] = weighted_score
            else:
                file_dict[filename] = score_file_diff(full_diff[4:],
                                                      relative_date,
                                                      file_dict[filename], fix)

    return [file_dict, absolute_fix_count, weighted_fix_count]


def fill_up_file_list(file_dict, full_file_list):
    for filename in full_file_list:
        if filename not in file_dict:
            file_dict[filename] = [0]


def cal_avg_top_ten_percent_loc(file_dict):
    """
    Returns files sorted by the 10% highest scored lines
    :param file_dict:
    :return:
    """
    average_score = {}
    for file, line_scores in file_dict.items():
        clean_scores = list(
            reversed(sorted([x for x in line_scores if x != 0])))
        tenth = int(round(len(clean_scores) / 10))
        if tenth:
            average_score[file] = sum(clean_scores[:tenth]) / tenth
        else:
            average_score[file] = sum(clean_scores[:tenth])
    sorted_scores = []
    for file in reversed(sorted(average_score, key=average_score.get)):
        sorted_scores.append([file, average_score[file]])
    return sorted_scores


def cal_average_file_score(file_dict):
    """
    Returns files sorted by average line score
    :param file_dict:
    :param file_list:
    :return: list: [file, average_score]
    """
    average_score = {}
    for file, line_scores in file_dict.items():
        if len(line_scores):
            average_score[file] = sum(line_scores) / len(line_scores)
        else:
            average_score[file] = 0
    sorted_scores = []
    for file in reversed(sorted(average_score, key=average_score.get)):
        sorted_scores.append([file, average_score[file]])
    return sorted_scores


def cal_bug_count(file_dict):
    sorted_scores = []
    for file in reversed(sorted(file_dict, key=file_dict.get)):
        sorted_scores.append([file, file_dict[file]])

    return sorted_scores


def cal_sorted_line_scores(file_dict):
    """

    :param file_dict:
    :return:
    """
    if not file_dict:
        return []
    raw_line_list = []
    for filename, line_score in file_dict.items():
        for score in line_score:
            if isinstance(score, list):
                raw_line_list.append([filename, 0])
            else:
                raw_line_list.append([filename, score])
    return list(reversed(sorted(raw_line_list, key=itemgetter(1))))


def cal_sorted_line_scores_for_print(file_dict):
    """

    :param file_dict:
    :return:
    """
    if not file_dict:
        return []
    raw_line_list = []
    for filename, line_score in file_dict.items():
        for line, score in enumerate(line_score):
            if isinstance(score, list):
                raw_line_list.append([0, filename, line])
            else:
                raw_line_list.append([score, filename, line])
    return list(reversed(sorted(raw_line_list, key=itemgetter(0))))


def count_project_lines():
    lines = {}
    for file in get_git_files():
        lines[file] = count_lines(file)
    return lines


def write_score_list_to_file(filename, fix_count):
    with open(filename, 'w', errors='ignore') as file:
        for name, count in fix_count:
            file.write(str(count) + " " + name + "\n")


def print_output(file_score_list, length, file=None):
    print("##################################################################")
    if len(file_score_list[0]) < 3:
        if length >= 0:
            for i, file_score in enumerate(file_score_list):
                print("{:10.6f}".format(
                    file_score[1]) + "  " + str(file_score[0]))
                if length != 0 and i > length-1:
                    break
        if file:
            write_score_list_to_file(file, file_score_list)
    else:
        if length >= 0:
            i = 0
            for score, name, line in file_score_list:
                print("{:7.6f}".format(score) + "  " +
                      name + " line: " + str(line))
                i += 1
                if length != 0 and i > length-1:
                    break
        # TODO file write


def main():
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__)

    parser.add_argument("-d", "--depth", type=int,
                        help="number of commits to be looked at", default=500)
    parser.add_argument("-g", "--grep", type=str,
                        help="non-case-sensitive regular expression"
                             " used to match commits",
                        default=".*([F|f]ix(e[s|d])?|[C|c]lose[s|d]?)")
    parser.add_argument("-p", "--path", type=str,
                        help="path to git repository",
                        default="./")
    parser.add_argument("-s", "--sorting", type=str,
                        help="choose the method of sorting:",
                        default='avg')
    parser.add_argument("-l", "--length", type=int,
                        help="number of entries to print, 0 for everything,"
                             " -1 for nothing",
                        default=0)
    parser.add_argument("-f", "--file", type=str,
                        help="filename to print the result to",
                        default=None)

    args = parser.parse_args()
    crawl_depth = args.depth
    regex = re.compile(args.grep, re.IGNORECASE)
    sort = args.sorting

    sorting_dir = {}
    sorting_dir['avg'] = cal_average_file_score
    sorting_dir['topten'] = cal_avg_top_ten_percent_loc
    sorting_dir['absolute'] = cal_bug_count
    sorting_dir['bugspots'] = cal_bug_count
    sorting_dir['lines'] = cal_sorted_line_scores_for_print

    if sort not in sorting_dir.keys():
        print("The given sorting is not supported")
        return 0

    workdir = os.getcwd()
    os.chdir(args.path)

    scores = get_line_bugspots(crawl_depth, regex)
    line_scores = scores[0]
    absolute_fix_counts = scores[1]
    weighted_fix_count = scores[2]

    output = None

    if sort == 'absolute':
        output = sorting_dir['absolute'](absolute_fix_counts)
    elif sort == 'bugspots':
        output = sorting_dir['bugspots'](weighted_fix_count)
    else:
        output = sorting_dir[sort](line_scores)

    print_output(output, args.length, args.file)

    os.chdir(workdir)

    return 1


if __name__ == '__main__':
    exit(main())
