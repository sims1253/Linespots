# Usage
Run `python3 linespoty.py` in your repository

There are also the following options available:
- `-d` or `--depth`: number of commits to be looked at", default=`500`
- `-g` or `--grep`: non-case-sensitive regular expression used to match commits, default=`".*([F|f]ix(e[s|d])?|[C|c]lose[s|d]?)"`
- `-p` or `--path`: path to the repository, default=`'./'`
- `-l` or `-- length`: number of entries to print, 0 for everything, -1 for nothing, default=`0`
- `-f` or `--file`: filename to print the result to, default=`None`
- `-s` or `--sporting`: method of sorting results, default=`'avg'`
    - `'avg'` for highest average line_score per file
    - `'high'` for highest line_score per file
    - `'avgclean'` for highest average line_score  per file excluding lines rated 0
    - `'avgcleanhead'` for highest average line_score per file excluding lines rated 0 and highest 10 lines
    - `'topten'` for highest 10% line_scores per file
    - `'absolute'` for absolute fix count per file
    - `'bugspots'` for weighted fix count per file. This is the Bugspots reference implementation


# Reproduce Thesis Evaluation
To reproduce the results presented in the thesis "A Line based Approach
for Bugspots" copy linespots.py and evaluate.py into the repository and
modify evaluate.py to use the following parameters in the main()
function (a fully automatic way of doing this will be offered soon):


### Gnome/evolution (https://github.com/GNOME/evolution)
 - `start_points = [10, 50, 100, 250, 500, 1000, 2000]`
 - `depths = [50, 100, 250, 500, 1000, 2000]`
 - `origin = "b83f3a49f4487143f212d810fe5852ca2a29209d"`
 - `grep_text = "(fix(ing|e[s|d])?)|bug"`


### More to come! 

## Then run `python3 evaluate.py` 


# Notes

## Reasoning for fileList and scoreList
The fileList is used as a lookup table for finding the corresponding
scoreLists. The actions performed on the fileList after initializing are
mostly searching the files scoreList and adding and removing files.
The number of add and remove operations are small compared to the
number of search operations. A hash table offers O(1) as average case
and O(n) as worst case for all three operations. As to how often the
worst case is reached relies on the hash function and how effective it
can prevent collisions.
The only alternative could be offered by B(+), AVL or RB-Trees as they
offer O(log(n)) for average and worst case.
Lists offer add and remove with O(1) but have O(n) for search and thus
are not suited.
After some reading it seems like AVL and RB trees perform better in
real life scenarios than B trees and RB trees are the most used ones.
As it is rather simple to switch Data structures in the code, I might
start using a hash table and after the algorithm works, try out a
version using AVL and one using a RB tree, profile them and choose based
on the results. Unless I deal with huge projects containing millions of
files this should not make a significant real world difference i think.

* After a first test run going ofer 500 commits of the coala repository
that took ~0.2 seconds optimization in this area is not needed.



The scoreList is used to store the line scores per file. There are two
forms of usage for this. While parsing a diff an entry for every line
existing in the file after changes are applied is appended to a new
file score list. So for this part there are only append operations.
The second part is that for every line that already existed before the
changes and for every line the algorithm recognizes as a modification
the corresponding line score from the old scoreList is appended to the
new scoreList.
The operations thus consist of append and access operations, where there
is an append every time and some of those also include an access.
The three data structures that look interesting are the array, the list
and the RB tree(as argued above it is the mostly used tree and all offer
the same big O).
The list seems to be the obvious fit with O(1) for append and O(n) for
access, which can be further improved by modifying it to a jump list for
example. The tree again would need testing to find out if the O(log(n))
access is worth the also O(log(n)) append.
The Array does not look promising at first with the O(n) append.
What saves it is the fact that the cost comes from going over the bound
of allocated memory and having to enlarge the space. For one append this
is very costly. In python this is worked around by allocating bigger
memory chunks every time the array is filled and needs more space.
The allocations happen fewer and the amortized worst case complexity for
a python list (which is implemented as an array) is given with O(1).
This means that the python list offers average and amortized worst case
complexity of O(1) for append and access.

__Todo:__ Investigate if using a collections.deque could improve over
the list if for every step in the diff a pop is performed so there are
only pop and append operations and everything is O(1) worst case.
This might make mod finding harder as the old list is destroyed while
iterating it.


## Test Repo Candidates List
### BugCache for Inspections: Hit or Miss?
- Apache httpd
- Nautilus
- Gimp
- Evolution
- Lucene

###  0-1 Programming Model-Based Method for Planning Code Review using
Bug Fix History
- Squirrel SQL Client
- Angry IP Scanner

### An Implementation of Just-In-Time Fault-Prone Prediction Technique
 Using Text Classifier
- Apache OpenJPA
- Apache James
- Eclipse Birt

__Todo:__ Read papers again and see which ones evaluation process I
might be able to reproduce best so I can use their results to compare
mine to.


## Evaluation Process
- Choose an __origin__ and multiple __start_point__ and __depth__ to check for:
    - [500, 1000, 2000] for both in this case
- For every __start_point__ do:
    - Git checkout __origin__
    - Run linespots(__start_point__)
    This gives the __future_fix_count__ from __start_point__ until __origin__
    - For  every depth do:
        - Git checkout __origin__~__start_point__
        - Run linespots(__depth__)
        - plot future_fix_count__ to loc relation depending on different sortings
        - Run reference implementation cleanup and save data
- Plot reference sorting against __future_fix_count__
- Compare reference hit density with own hit density

