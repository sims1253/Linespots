from __future__ import division

from contextlib import contextmanager
from scipy.integrate import trapz
import numpy as np
import linespots
import re
import matplotlib.pyplot as plt
import os
from operator import itemgetter
from timeit import default_timer as timer
from collections import defaultdict


def future_score_diff_hunk(hunk_text, old_hunk_scores, fix):
    """

    :param hunk_text:
    :param old_hunk_scores:
    :param fix:
    :return:
    """
    fix_score = 0
    new_line_scores = []
    hunk_text.pop(0)
    line_nr = 0
    for line in hunk_text:
        if line.startswith("-"):
            if old_hunk_scores[line_nr:line_nr + 1]:
                if fix and old_hunk_scores[line_nr] > fix_score:
                    fix_score = old_hunk_scores[line_nr]
            line_nr += 1
        elif line.startswith("+"):
            new_line_scores.append(0)

        else:
            if line == "\ No newline at end of file":
                pass
            else:
                try:
                    new_line_scores.append(old_hunk_scores[line_nr])
                except:
                    new_line_scores.append(0)
            line_nr += 1
    return [new_line_scores, fix_score]


def future_score_file_diff(file_diff, line_scores, fix):
    """

    :param file_diff: Git diff of one file
    :param line_scores: The line_scores list for the file
    :param fix: true if the current commit is a bugfix
    :return: updated line_scores
    """
    fix_score = 0
    if not line_scores:
        return [[], fix_score]

    else:
        new_line_scores = []
        current_line = 0
        for hunk in linespots.get_chunks_by_line_start(file_diff, "@@"):
            sub_location, sub_length, add_location, add_length = \
                linespots.get_hunk_meta(hunk[0])

            if sub_location - 1 > current_line:
                new_line_scores.extend(
                    line_scores[current_line: sub_location - 1])
                current_line += ((sub_location - 1) - current_line)

            scores = future_score_diff_hunk(
                hunk, line_scores[
                      sub_location - 1:sub_location + sub_length - 1], fix)
            new_line_scores.extend(scores[0])
            current_line += sub_length
            if scores[1] > fix_score:
                fix_score = scores[1]

        if current_line < len(line_scores):
            new_line_scores.extend(line_scores[current_line:])

        return [new_line_scores, fix_score]


def future_find_fixes(depth, grep, past_file_dict):
    """
    :param depth: number of commits to look at
    :param grep: search term to find fix commits
    :param past_file_dict:
    :return: [(dict: filename, line_score),(dict: filename, fix_count)]
    """
    future_dict = past_file_dict
    all_commits = linespots.get_full_commits(depth)
    oldest = int(all_commits[-1][1].strip('\''))
    repo_age = int(all_commits[0][1].strip('\'')) - oldest
    fix_score_list = []
    renames = 0
    news = 0
    deletes = 0
    for commit in reversed(all_commits):
        fix_score = 0
        commit_date = int(commit[1])
        commit_body, commit_diff = linespots.separate_body_diff(commit[2:])
        fix = linespots.is_fix_commit(commit_body, grep)
        relative_date = linespots.normalize_timestamp(
            commit_date, oldest, repo_age)

        for full_diff in linespots.get_chunks_by_line_start(
                commit_diff, "diff --git"):
            filename = full_diff[0].split()[3][2:]

            if full_diff[1].startswith("new file mode"):
                news += 1

            elif full_diff[1].startswith("deleted file mode"):
                deletes += 1

            elif full_diff[2].startswith("rename from"):
                renames += 1
                try:
                    oldfile = full_diff[2].split()[2]
                    scoring = future_score_file_diff(
                        full_diff[4:], future_dict.pop(oldfile), fix)
                    future_dict[filename] = scoring[0]
                    if scoring[1] > fix_score:
                        fix_score = scoring[1]

                except KeyError:
                    pass

            else:
                scoring = future_score_file_diff(
                    full_diff[4:], future_dict[filename], fix)
                future_dict[filename] = scoring[0]
                if scoring[1] > fix_score:
                    fix_score = scoring[1]
            if fix:
                fix_score_list.append(fix_score)
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print(news, " News were Made")
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print(deletes, " Deletes were Made")
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print(renames, " Renames were Made")
    return fix_score_list


def read_lines_from_file(filename):
    """
    Returns a list of lines from a given file
    :param filename:
    :return:
    """
    with open(filename, 'r', errors='ignore') as file:
        return file.readlines()


def calculate_closed_density(score_list, fix_count,
                             fix_sum, line_counts, line_sum, ):
    """
    :param score_list: dict: filename, line scores
    :param fix_count: dict: filename, fix count
    :param fix_sum: Sum of fixes for the whole project
    :param line_counts: dict: filename, line count
    :param line_sum: Sum of lines for the whole project
    :return
    """
    edit_lines = 0
    edit_fix_count = 0
    print_data = []
    print_data.append((0, 0))

    for file, score in score_list:
        if file in line_counts:
            line_number = line_counts[file]
            if line_number > 0:
                if line_sum * fix_sum > 0:
                    edit_lines += line_number
                    edit_fix_count += fix_count[file]
                    # lines_of_code, number_of_fixes,
                    print_data.append((edit_lines / line_sum,
                                       edit_fix_count / fix_sum))
    print_data.append((1, print_data[-1][1]))
    return print_data


def rank_density_by_auc(sortings_list):
    auc_list = []
    for name, print_data in sortings_list:
        number_of_fixes = [field[1] for field in print_data]
        abs_lines_of_code = [field[0] for field in print_data]

        hit_density_list = []
        for lines, nof in print_data:
            if lines > 0:
                hit_density_list.append(nof / lines)
            else:
                hit_density_list.append(0)

        for index, line in enumerate(abs_lines_of_code):
            if line > 0.2:
                auc_list.append(
                    [name, trapz(number_of_fixes, abs_lines_of_code),
                     trapz(number_of_fixes[:index], abs_lines_of_code[:index]),
                     trapz(hit_density_list, abs_lines_of_code),
                     trapz(hit_density_list[:index],
                           abs_lines_of_code[:index]),
                     max(hit_density_list),
                     abs_lines_of_code[
                         hit_density_list.index(max(hit_density_list))]])
                # name, aucec, aucec20, auhdc, auhdc20, maxhd, maxhdindex
                break
    return list(reversed(sorted(auc_list, key=itemgetter(2))))


def print_closed_density(print_data, name, title, fix_sum):
    """

    :param print_data: [[lines_of_code], [number_of_fixes], [rel_lines_of_code]]
    :param name: name of the pdf
    :param title: title for the plot
    :param fix_sum: sum of all fixes made in the future
    :return:
    """
    abs_lines_of_code = [field[0] for field in print_data]
    number_of_fixes = [field[1] for field in print_data]
    hit_density_list = [0]
    hit_density_list += [nof / abs for abs, nof in print_data if abs > 0]

    figure = plt.figure()
    loc_to_bugs = figure.add_subplot(111)
    loc_to_bugs.plot(abs_lines_of_code, number_of_fixes, 'b-')
    loc_to_bugs.set_xlabel('proportion of Lines of Code [0,1]')
    loc_to_bugs.set_ylabel('proportion of Bugs Hit [0,1]', color='b')
    for tl in loc_to_bugs.get_yticklabels():
        tl.set_color('b')

    hit_density = loc_to_bugs.twinx()
    hit_density.plot(abs_lines_of_code, hit_density_list, 'r--')
    hit_density.set_ylabel('Hit Density', color='r')
    hit_density.grid(None)
    for tl in hit_density.get_yticklabels():
        tl.set_color('r')

    loc_to_bugs.set_ylim(0, 1.02)
    loc_to_bugs.set_xlim(0, 0.15)

    plt.title(title + "\n" + str(fix_sum) + " Bugs were fixed in the future")
    plt.grid(True)
    plt.savefig(name + ".pdf")

    loc_to_bugs.set_ylim(0, 1.02)
    loc_to_bugs.set_xlim(0, 1)

    plt.title(title + "\n" + str(fix_sum) + " Bugs were fixed in the future")
    plt.grid(True)
    plt.savefig(name + "-big.pdf")
    plt.close()


@contextmanager
def print_time(format_str, *format_args):
    """
    :param format_str: Contains one keyword placeholder named time where the
                       time will be inserted.
    :param format_args: Args to pass on to format.
    :return:
    """
    start = timer()
    try:
        yield
    finally:
        print(format_str.format(*format_args, time=timer() - start))


def print_line_ranking(print_data):
    """

    :param print_data:
    :return:
    """
    hit_density_list = []
    for lines, fixes in zip(print_data[1], print_data[2]):
        if lines > 0:
            hit_density_list.append(fixes / lines)
        else:
            hit_density_list.append(0)

    figure = plt.figure()
    loc_to_bugs = figure.add_subplot(111)
    loc_to_bugs.plot(print_data[1], print_data[2], 'b-')
    loc_to_bugs.set_xlabel('proportion of Lines of Code [0,1]')
    loc_to_bugs.set_ylabel('proportion of Bugs Hit [0,1]', color='b')
    for tl in loc_to_bugs.get_yticklabels():
        tl.set_color('b')

    hit_density = loc_to_bugs.twinx()
    hit_density.plot(print_data[1], hit_density_list, 'r--')
    hit_density.set_ylabel('Hit Density', color='r')
    hit_density.grid(None)
    for tl in hit_density.get_yticklabels():
        tl.set_color('r')

    loc_to_bugs.set_ylim(0, 1.02)
    loc_to_bugs.set_xlim(0, 0.2)

    plt.title("Line Based Results" + "\n" + str(
        print_data[0]) + " Bugs were fixed in the 'future'")
    plt.grid(True)
    plt.savefig("Line_Based_Results.pdf")

    loc_to_bugs.set_ylim(0, 1.02)
    loc_to_bugs.set_xlim(0, 1)

    plt.title("Line Based Results" + "\n" + str(
        print_data[0]) + " Bugs were fixed in the 'future'")
    plt.grid(True)
    plt.savefig("Line_Based_Results_big.pdf")
    plt.close()


def rank_lines(origin, past_line_scores, depth, grep, line_sum):
    """

    :param origin:
    :param past_line_scores:
    :param depth:
    :param grep:
    :param line_sum:
    :return:
    """
    linespots.git_checkout_branch(origin)
    fix_list = list(
        reversed(sorted(future_find_fixes(depth, grep, past_line_scores))))
    sorted_lines = linespots.cal_sorted_line_scores(past_line_scores)
    fix_sum = len(fix_list)
    print_data = []
    print_data.append(fix_sum)
    print_data.append([])
    print_data.append([])
    print_data[1].append(0)
    print_data[2].append(0)

    fixes_reached = 0
    lines_reached = 0
    last_score = sorted_lines[0][1]
    for file_name, line_score in sorted_lines:
        lines_reached += 1
        if line_score < last_score:
            if last_score <= fix_list[fixes_reached]:
                fixes_reached += 1
            last_score = line_score
        print_data[1].append(lines_reached / line_sum)
        print_data[2].append(fixes_reached / fix_sum)

    print_data[1].append(1)
    if len(print_data[2]):
        print_data[2].append(print_data[2][-1])
    else:
        print_data[2].append(0)

    hit_density_list = []
    for lines, fixes in zip(print_data[1], print_data[2]):
        if lines > 0:
            hit_density_list.append(fixes / lines)
        else:
            hit_density_list.append(0)

    auc = []
    for index, line in enumerate(print_data[1]):
        if line > 0.2:
            max_hit = max(hit_density_list) if hit_density_list else 0
            max_index = len(print_data[1])-1
            try:
                max_index = hit_density_list.index(max(hit_density_list))
            except:
                pass
            auc.append(
                ["Line Ranking", trapz(print_data[2], print_data[1]),
                 trapz(print_data[2][:index], print_data[1][:index]),
                 trapz(hit_density_list, print_data[1]),
                 trapz(hit_density_list[:index], print_data[1][:index]),
                 max_hit, print_data[1][max_index]])
            break
    return [print_data, list(reversed(sorted(auc, key=itemgetter(2))))]


def measure(depth, start_param,
            regex, project_path,
            future_absolute_fix_count,
            line_counts, line_sum, fix_sum, origin, lines):
    """

    :param depth:
    :param start_param:
    :param regex:
    :param project_path:
    :param future_absolute_fix_count:
    :param line_counts:
    :param line_sum:
    :param fix_sum:
    :return:
    """
    with print_time("Linespots with depth {:4d} took {time:10.3f} seconds",
                    depth):
        scores = linespots.get_line_bugspots(depth, regex)

    sortings_set = []

    if not lines:
        with print_time("Bugspots with depth {:4d} took {time:10.3f} seconds",
                        depth):
            raw_reference = linespots.get_file_bugspots(depth, regex)

        sorted_reference = [
            [file, raw_reference[file]]
            for file in reversed(sorted(raw_reference, key=raw_reference.get))]

    depth_dir = project_path + "/linespots/-" + \
        str(start_param) + "/-" + str(depth)
    os.makedirs(depth_dir)
    os.chdir(depth_dir)
    if not lines:
        def dot_the_stuff(score_list, title: str):
            name = title.replace(' ', '_')
            density = calculate_closed_density(
                score_list,
                fix_count=future_absolute_fix_count,
                fix_sum=fix_sum,
                line_counts=line_counts,
                line_sum=line_sum)
            print_closed_density(density, name, title, fix_sum)
            linespots.write_score_list_to_file(title, score_list)
            sortings_set.append([title, density])

        dot_the_stuff(linespots.cal_average_file_score(scores[0]),
                      "Average")
        dot_the_stuff(linespots.cal_avg_top_ten_percent_loc(scores[0]),
                      "Top ten percent")
        dot_the_stuff(linespots.cal_bug_count(scores[2]),
                      "Bugspots with tracking")
        dot_the_stuff(sorted_reference, "Bugspots")

        ranked_auc = rank_density_by_auc(sortings_set)
        write_auc_to_file(ranked_auc)

        sorted_fix_count = []
        for file in reversed(sorted(future_absolute_fix_count,
                                    key=future_absolute_fix_count.get)):
            sorted_fix_count.append([file, future_absolute_fix_count[file]])
        linespots.write_score_list_to_file("fix_count", sorted_fix_count)

    if lines:
        ranked_lines = rank_lines(origin, scores[0], start_param, regex,
                                  line_sum)
        print_line_ranking(ranked_lines[0])
        os.chdir(project_path)
        return ranked_lines[1]
    else:

        os.chdir(project_path)

        return ranked_auc


def rank_auc(auc_list):
    unfolded_list = []
    for start, depth_list in auc_list:
        for depth, measure_results in depth_list:
            for name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in measure_results:
                unfolded_list.append(
                    [start, depth, name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines])
    return list(reversed(sorted(unfolded_list, key=itemgetter(3))))


def rank_medium_auc(auc_list):
    unfolded_list = []
    for depth, measure_results in auc_list[1]:
        for name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in measure_results:
            unfolded_list.append(
                [auc_list[0], depth, name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines])
    return list(reversed(sorted(unfolded_list, key=itemgetter(3))))


def write_auc_to_file(sorted_auc):
    with open('auc-ranking.txt', 'w', errors='ignore') as file:
        file.write(
            "Name & AUCEC & AUCEC20 &  AUHDC & AUHDC20 & Max. HD & Max. HD Index \\\\ \n")
        for name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in sorted_auc:
            file.write("{:7.6f} & {:7.6f} & {:7.6f} & {:7.6f} & {:7.6f} & {:16.16f} & ".format(
                absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines) + name + " \\\\ \n")


def write_long_auc_to_file(filename, sorted_auc):
    aucec = defaultdict(list)
    aucec20 = defaultdict(list)
    auhdc = defaultdict(list)
    auhdc20 = defaultdict(list)
    maxhd = defaultdict(list)
    maxhdindex = defaultdict(list)
    with open(filename, 'w', errors='ignore') as file:
        file.write(
            "Name & AUCEC & AUCEC20 &  AUHDC & AUHDC20 & Max. HD & Max. HD Index \\\\ \n")
        for start, depth, name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in sorted_auc:
            aucec[name].append(absolute_trapz)
            aucec20[name].append(twenty_trapz)
            auhdc[name].append(hit_area)
            auhdc20[name].append(hit_twenty)
            maxhd[name].append(hit_max)
            maxhdindex[name].append(density_lines)
            file.write("{:5d} & {:5d} & {:7.6f} & {:7.6f} & {:7.6f} & {:7.6f} & {:7.3f} & {:16.16f} & ".format(
                start, depth, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max,
                density_lines) + name + " \\\\ \n")

        file.write(
            "\nName & AUCEC Mean & AUCEC Std. Dev. & AUCEC20 Mean & AUCEC20 Std. Dev. \\\\ \n")
        for name in aucec.keys():
            file.write(
                "{:} & {:6.6f} & {:6.6f} & {:6.6f} & {:6.6f} \\\\ \n".format(name, np.mean(aucec[name]), np.std(aucec[name]), np.mean(aucec20[name]), np.std(aucec20[name])))

        file.write(
            "\nName & AUHDC Mean & AUHDC Std. Dev. & AUHDC20 Mean & AUHDC20 Std. Dev. \\\\ \n")
        for name in aucec.keys():
            file.write(
                "{:} & {:6.6f} & {:6.6f} & {:6.6f} & {:6.6f} \\\\ \n".format(name, np.mean(auhdc[name]), np.std(auhdc[name]), np.mean(auhdc20[name]), np.std(auhdc20[name])))

        file.write(
            "\nName & Max. HD Mean & Max. HD Std. Dev. & Max. HD Index Mean & Max. HD Index Std. Dev.\\\\ \n")
        for name in aucec.keys():
            file.write(
                "{:} & {:7.3f} & {:7.3f} & {:16.16f} & {:16.16f} \\\\ \n".format(name, np.mean(maxhd[name]), np.std(maxhd[name]), np.mean(maxhdindex[name]), np.std(maxhdindex[name])))


def paint_start_depth_area(sorted_auc):
    print_data = defaultdict(list)
    for start, depth, name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in sorted_auc:
        if print_data[name] == []:
            print_data[name].append([])
            print_data[name].append([])
            print_data[name].append([])
            print_data[name].append([])
        print_data[name][0].append(depth)
        print_data[name][1].append(start)
        print_data[name][2].append(absolute_trapz)
        print_data[name][3].append(hit_max)

    aucec = defaultdict(list)
    aucec20 = defaultdict(list)
    auhdc = defaultdict(list)
    auhdc20 = defaultdict(list)
    maxhd = defaultdict(list)
    maxhdindex = defaultdict(list)
    for start, depth, name, absolute_trapz, twenty_trapz, hit_area, hit_twenty, hit_max, density_lines in sorted_auc:
        aucec[name].append(absolute_trapz)
        aucec20[name].append(twenty_trapz)
        auhdc[name].append(hit_area)
        auhdc20[name].append(hit_twenty)
        maxhd[name].append(hit_max)
        maxhdindex[name].append(density_lines)

    plt.figure()
    plt.boxplot(list(aucec.values()), labels=list(aucec.keys()), showfliers=False)
    plt.title("AUCEC Box Plot")
    plt.savefig("aucec_box.pdf")
    plt.close()

    plt.figure()
    plt.boxplot(list(aucec20.values()), labels=list(aucec20.keys()), showfliers=False)
    plt.title("AUCEC 20 Box Plot")
    plt.savefig("aucec20_box.pdf")
    plt.close()

    plt.figure()
    plt.boxplot(list(auhdc.values()), labels=list(auhdc.keys()), showfliers=False)
    plt.title("AUHDC Box Plot")
    plt.savefig("auhdc_box.pdf")
    plt.close()

    plt.figure()
    plt.boxplot(list(auhdc20.values()), labels=list(auhdc20.keys()), showfliers=False)
    plt.title("AUHDC 20 Box Plot")
    plt.savefig("auhdc20_box.pdf")
    plt.close()

    plt.figure()
    plt.boxplot(list(maxhd.values()), labels=list(maxhd.keys()), showfliers=False)
    plt.title("Max. Hit Density Box Plot")
    plt.savefig("maxhd_box.pdf")
    plt.close()

    plt.figure()
    plt.boxplot(list(maxhdindex.values()), labels=list(maxhdindex.keys()), showfliers=False)
    plt.title("Max. Hit Density Index Box Plot")
    plt.savefig("maxhdindex_box.pdf")
    plt.close()

    for name, points in print_data.items():
        x = np.asarray(points[0])
        y = np.asarray(points[1])
        z = np.asarray(points[2])
        a = np.asarray(points[3])

        plt.scatter(x, y, c=z, s=75)

        plt.xlabel("depth")
        plt.ylabel("start")
        plt.colorbar()
        plt.savefig(name.replace(' ', '_') + "_area.pdf")
        plt.close()

        plt.scatter(x, y, c=a, s=75)

        plt.xlabel("depth")
        plt.ylabel("start")
        plt.colorbar()
        plt.savefig(name.replace(' ', '_') + "density.pdf")
        plt.close()


def main():
    """
    Handles the argument passing via command line
    :return:
    """

    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__)

    parser.add_argument('-d', '--depth', nargs='+', type=int,
                        help="number of commits to be looked at",
                        default=[10, 25, 50, 100, 250, 500, 1000])
    parser.add_argument("-g", "--grep", type=str,
                        help="non-case-sensitive regular expression"
                             " used to match commits",
                        default="(fix(ing|e[s|d])?)|bug")
    parser.add_argument("-o", "--origin", type=str,
                        help="commit sha used as evaluation start point",
                        default=None)
    parser.add_argument('-f', '--future', nargs='+', type=int,
                        help="size of futures to evaluate against",
                        default=[10, 25, 50, 75, 100, 125, 150, 200, 250, 300,
                                 350])
    parser.add_argument("--short", type=bool,
                        help="True if only depth>future should be checked",
                        default=False)
    parser.add_argument("--line", type=bool,
                        help="True if line result profiling shall be done",
                        default=False)

    args = parser.parse_args()

    start_points = args.future
    depths = args.depth
    origin = args.origin
    grep_text = args.grep
    regex = re.compile(grep_text, re.IGNORECASE)
    workdir = os.getcwd()
    if os.path.isdir('./linespots'):
        from shutil import rmtree
        rmtree('./linespots')
    area_under_curve_list = []
    area_under_curve_lines_list = []

    for start in start_points:
        start_level_auc = []
        start_level_lines_auc = []
        linespots.git_checkout_branch(origin)

        absolute_fix_counts = linespots.get_line_bugspots(
            start, re.compile(grep_text, re.IGNORECASE))[1]

        for depth in depths:
            linespots.git_checkout_branch(origin + "~" + str(start))

            line_counts = linespots.count_project_lines()
            line_sum = sum(line_counts.values())
            fix_sum = sum(absolute_fix_counts.values())
            if args.short and depth > start:
                measurement = measure(depth, start,
                                      regex, workdir,
                                      absolute_fix_counts,
                                      line_counts,
                                      line_sum, fix_sum, origin, args.line)
                if args.line:
                    start_level_lines_auc.append([depth, measurement])
                else:
                    start_level_auc.append([depth, measurement])
            elif not args.short:
                measurement = measure(depth, start,
                                      regex, workdir,
                                      absolute_fix_counts,
                                      line_counts,
                                      line_sum, fix_sum, origin, args.line)
                if args.line:
                    start_level_lines_auc.append([depth, measurement])
                else:
                    start_level_auc.append([depth, measurement])

        if args.line:
            area_under_curve_lines_list.append([start, start_level_lines_auc])
        else:
            area_under_curve_list.append([start, start_level_auc])
        os.chdir(workdir + "/linespots/-" + str(start))

        if args.line:
            write_long_auc_to_file("long_lines_auc", rank_medium_auc(
                [start, start_level_lines_auc]))
        else:
            write_long_auc_to_file(
                "long_auc-ranking", rank_medium_auc([start, start_level_auc]))
        os.chdir(workdir)

    os.chdir(workdir + "/linespots")
    if args.line:
        paint_start_depth_area(rank_auc(area_under_curve_lines_list))
        write_long_auc_to_file(
            "long_lines_auc, ", rank_auc(area_under_curve_lines_list))
    else:
        paint_start_depth_area(rank_auc(area_under_curve_list))
        write_long_auc_to_file("long_auc-ranking",
                               rank_auc(area_under_curve_list))
    os.chdir(workdir)
    return 1


if __name__ == '__main__':
    exit(main())
